# parameters used by the different modules
perception:
  image_grabber:
    ports_dict: {"101": 5002, "102": 5004, "103": 5006, "104": 5008, "105": 5010,
                "140": 5080, "141": 5082, "142": 5084, "143": 5086, "144": 5088, "145": 5090,
                "146": 5092, "147": 5094, "148": 5096, "149": 5098, "150": 5100, "151": 5102, "152": 5104, "153": 5106}
    camera_type_dict: {"101": "m3116", "102": "m3116", "103": "m3116", "104": "m3116", "105": "m3116",
                      "140": "p1378", "141": "p1378", "142": "p1378", "143": "p1378", "144": "p1378", "145": "p1378",
                      "146": "p1378", "147": "p1378", "148": "p1378", "149": "p1378", "150": "p1378", "151": "p1378",
                      "152": "p1378", "153": "m3116"}
    raw_image_reshaped: [0, 0]
    compression_quality: 95
    max_wait_time: 0.8 # after no motion during max_wait_time, the next image is guaranteed to be analyzed regardless of motion
  tag_detector:
    aruco_dict:
      "4x4_1000"
    aruco_params:
      "standard"
    plot:
      False
    image_shape_resized:
      [0, 0]

  semantic_segmentation:
    contour_min_length:
      60
    image_shape_resized:
      [1024, 576]
    weight_path:
      # "BiSeNet/res/tuve_0505/tuve_0505_final.pth"
      "BiSeNet2022/res/tuve_model_temp3.pth"
    plot:
      True
    confidence_threshold: 0.90
    sem_seg_dilation: 7

  overlapping_area_fusion:
    tag_history_length: 1.0
    contour_min_length: 30
    robot_padding: 0.50 # padding the robot with ## cm to make sure it is not detected as an obstacle
    object_padding: 4 # padding objects with a 'boarder' of object_padding pixels width. E.g., if grid_resolution is 5 cm and object padding is 2, the object grows with 10 cm in each direction
    occupancy_grid_resolution: 0.05 # resolution of the occupancy grid in cm. The grid is defined by squares of the specified size
    top_left_corner: [4.0, 12.0, 0.0, 1.0]  # TODO hardcoded map size
    bottom_right_corner: [60.0, -6.0, 0.0, 1.0]
    object_low_pass_value: 1 # defines the number of updates an object needs to be consistent to be considered "not noise"
    adjacency_dict: {"102": ["103","104"],
                    "104": ["102", "103"],
                    "103": ["102","104"],
                    "140": ["141"],
                    "141": ["140", "142"],
                    "142": ["141", "143"],
                    "143": ["142", "144"],
                    "144": ["143", "145"],
                    "145": ["144", "146"],
                    "146": ["145", "147"],
                    "147": ["146", "148", "149"],
                    "148": ["147", "149"],
                    "149": ["147" ,"148", "150"],
                    "150": ["149", "151", "152", "153"],
                    "151": ["150", "152"],
                    "152": ["151", "150", "153"],
                    "153": ["152", "150"]
                    }
    atr_save_duration: 5.0 # number of seconds that the atr detections are saved.
    plot: True


# general parameters
robots:
  geometry:
    width: 0.56
    length: 0.72
    height: 0.74
    # height: 0.24

tags:
  robots:
    ids: [10, 12, 16]
    size: 0.098
  poles:
    ids: [18]
    height: 0.915 # ?

launch:
  semantic_segmentation:
    # cam_ids: ["102","104"]
    # cam_ids: ["140", "141", "142", "143", "144", "145", "146","147", "148", "149", "150", "151", "152"]
    cam_ids: ["145","146","147", "149", "150", "151", "152"]
    timer_period: 0.1

  tag_detector:
    # cam_ids: [102, 104]
    # cam_ids: ["140", "141", "142", "143", "144", "145", "146","147", "148", "149", "150", "151", "152"]
    cam_ids: ["145","146","147", "149", "150", "151", "152", "153"]
    # cam_ids: ["152"]
    timer_period_tag: 0.01
    timer_period_image: 0.01
    timer_period_motion_detector: -1.0
    min_wait_time_tag: 0.01 # never do tag detection faster than min_wait_time_tag
    min_wait_time_image: 0.01 # never send a new image faster than min_wait_time_image


  cam_to_factory_transform:
    # cam_ids: [102, 104]
    # cam_ids: [140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152]
    cam_ids: [145, 146, 147, 149, 150, 151, 152, 153]
    # cam_ids: [145, 146]
    timer_period: 0.01 # not used
    object_class: [6, 0] #6 for AR_tags, 0 for semantic segmentation objects

  tag_fusion:
    timer_period: 0.01
    area_ids: [0]
    # cam_ids_per_area: [["102", "103", "104"]] # list of camera ids that is within each area
    # cam_ids_per_area: [["140", "141", "142", "143", "144", "145", "146","147", "148", "149", "150", "151", "152", "153"]] # list of camera ids that is within each area
    cam_ids_per_area: [["145","146","147", "149", "150", "151", "152", "153"]]
    # cam_ids_per_area: [["145","146"]]

  atr_pose_fusion:
    timer_period: 0.01
    area_ids: [0]
    # cam_ids_per_area: [["102", "103", "104"]] # list of camera ids that is within each area
    # cam_ids_per_area: [["140", "141", "142", "143", "144", "145", "146","147", "148", "149", "150", "151", "152", "153"]] # list of camera ids that is within each area
    cam_ids_per_area: [["10", "12", "16"]]

  overlapping_area_fusion:
    timer_period: 0.1
    area_ids: [0]
    # cam_ids_per_area: [["102", "104"]] # list of camera ids that is within each area
    # cam_ids_per_area: [[141, 142, 143, 145, 146, 147, 149, 150, 151, 152]] # list of camera ids that is within each area
    cam_ids_per_area: [[145, 146, 147, 149, 150, 151, 152]] # list of camera ids that is within each area
    tag_history_length: 1.0

  tag_and_object_detector:
    cam_ids: ["102", "104"]
    timer_period: 0.001
